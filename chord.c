#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TAGINIT 0
#define TAGSR   1
#define TAGRES  2
#define NB_PROC 6
#define NB_BITS 6
#define CLE 50

int compar(const void* x, const void* y)
{
    return ( *(int*)x - *(int*)y ) ;
}

void simulateur(void)
{
    int id_max = (int)pow(2, NB_BITS);

    int clePair[NB_PROC];
    int i;
    
    srand(getpid());

    for (i = 0; i < NB_PROC; i++) {
        clePair[i] = rand() % id_max;
        int j;
        for (j = 0; j < i; j++) {
            while (clePair[j] == clePair[i])
                clePair[i] = rand() % id_max;
        }
        fprintf(stderr, "%d\n", clePair[i]);
    }

    qsort(clePair, NB_PROC, sizeof(int), compar);

    for (i = 0; i < NB_PROC; i++) {
        fprintf(stderr, "%d\n", clePair[i]);
    }
    
    for (i = 0; i < NB_PROC; i++) {
        MPI_Send(&clePair[i], 1, MPI_INT, i, TAGINIT, MPI_COMM_WORLD);
        MPI_Send(&clePair[(i+NB_PROC-1) % NB_PROC], 1, MPI_INT, i, TAGINIT, MPI_COMM_WORLD);
        MPI_Send(&clePair[(i+1) % NB_PROC], 1, MPI_INT, i, TAGINIT, MPI_COMM_WORLD);
    }
}

void recherche(int rang, int cleDonnee, int clePair)
{
    int cle;
    MPI_Status status;
    MPI_Recv(&cle, 1, MPI_INT, (rang-1+NB_PROC) % NB_PROC, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    switch (status.MPI_TAG) {
        case TAGSR:          
            if ((cle >= cleDonnee) && (cle <= clePair)) {
                fprintf(stderr, "Trouvé P%d\n", rang);
                MPI_Send(&cle, 1, MPI_INT, (rang+1) % NB_PROC, TAGRES, MPI_COMM_WORLD);
            } else {
                MPI_Send(&cle, 1, MPI_INT, (rang+1) % NB_PROC, TAGSR, MPI_COMM_WORLD);
            }
        case TAGRES:
            MPI_Send(&cle, 1, MPI_INT, (rang+1) % NB_PROC, TAGRES, MPI_COMM_WORLD);
    }
}

int init_recherche(int rang, int cleDonnee, int clePair)
{
    int cle = CLE;
    MPI_Status status;
    if ((CLE >= cleDonnee) && (CLE <= clePair)) {
        fprintf(stderr, "Trouvé P%d\n", rang);
        return 1;
    }
    MPI_Send(&cle, 1, MPI_INT, (rang+1) % NB_PROC, TAGSR, MPI_COMM_WORLD);
    MPI_Recv(&cle, 1, MPI_INT, (rang-1+NB_PROC) % NB_PROC, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    if (status.MPI_TAG == TAGRES)
        return 1;
    else
        return 0;
}

void p2p(int rang) // recherche -> insertion -> recherche -> suppression -> recherche
{
    int clePair;
    int cleDonnee;
    int cleSucc;
    MPI_Status status;

    MPI_Recv(&clePair, 1, MPI_INT, NB_PROC, TAGINIT, MPI_COMM_WORLD, &status);
    MPI_Recv(&cleDonnee, 1, MPI_INT, NB_PROC, TAGINIT, MPI_COMM_WORLD, &status);
    MPI_Recv(&cleSucc, 1, MPI_INT, NB_PROC, TAGINIT, MPI_COMM_WORLD, &status);
    cleDonnee++;
    
    fprintf(stderr, "P%d: %d, %d, %d\n", rang, clePair, cleDonnee, cleSucc);

    if (rang == 2)
        if (init_recherche(rang, cleDonnee, clePair))
            fprintf(stderr, "Trouvé\n");
    else
        recherche(rang, cleDonnee, clePair);
}

int main(int argc, char* argv[])
{
    int nb_proc,rang;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nb_proc);

    if (nb_proc != NB_PROC+1) {
      printf("Nombre de processus incorrect !\n");
      MPI_Finalize();
      exit(2);
    }

    MPI_Comm_rank(MPI_COMM_WORLD, &rang);

    if (rang == NB_PROC) {
      simulateur();
    } else {
      p2p(rang);
    }

    MPI_Finalize();
    return 0;
}

